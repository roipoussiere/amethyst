module framagit.org/roipoussiere/amethyst

go 1.13

replace github.com/faiface/pixel => ./pixel

require (
	github.com/elastic/go-sysinfo v1.4.0
	github.com/faiface/pixel v0.10.0-beta
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/kirsle/configdir v0.0.0-20170128060238-e45d2f54772f
	github.com/llgcode/draw2d v0.0.0-20200603164053-19660b984a28
	github.com/otiai10/copy v1.2.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.3
	golang.org/x/image v0.0.0-20200801110659-972c09e46d76
	gopkg.in/go-playground/colors.v1 v1.2.0
	gopkg.in/yaml.v2 v2.3.0
)
