#!/usr/bin/env bash
# dependency: libnotify (ie. sudo apt install libnotify-bin)

set -e
MODULE_ID="tests.api"

if [ $# != 1 ]; then
	>&2 echo "Usage: amethyst $MODULE_ID <duo|quadro|octo|error|...>"
	exit 1
fi

json_path="`dirname "$0"`/$1.json"

if [ ! -f $json_path ]; then
	>&2 echo "Can not find json file $json_path."
	exit 1
fi

echo `cat "$json_path"`

while read input; do
	notify-send "You selected the ${input} facet."
done
