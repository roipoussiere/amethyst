# Contributing to Amethyst

Thank you for looking at this page! <3

There are many ways to contribute to Amethyst.

## Make this project known

- add a star on [the Gitlab repository](https://framagit.org/roipoussiere/amethyst);
- share this project on you favorite social media;
- talk about Amethyst to your family and friends.

## Discuss

You can participate on the [Discord server dedicated to Amethyst](https://discord.gg/UW53qGz), whether it's to provide
feedback, share your modules, say thanks, ask technical questions, talk to Amethyst developers, or anything else related
to Amethyst.

## Provide feedback

Want to report a bug or suggest a feature? There are various ways to provide feedback:

- [create a new issue](https://framagit.org/roipoussiere/amethyst/issues/new) on the Gitlab repository;
- [send an email](mailto:gitlab-incoming+f28cd5faccb6d05d82c4cf6198e20572@framagit.org);
- [contact me on Mastodon](https://mastodon.tetaneutral.net/@roipoussiere);
- discuss on the Discord server (see above).

## Be creative

Amethyst is designed to be highly customisable, letting users to:
- change appearance by editing a **configuration file**, or creating new **color schemes**.
- add new behaviors by creating custom **modules**;

### The resources folder

Before starting this fascinating customization process, you must know the user resources folder.

You can get it with `amethyst --config`. It should be:

- `$XDG_CONFIG_HOME/amethyst` or `$HOME/.config/amethyst` on **Linux**;
- `$HOME/Library/Application Support/amethyst` on **macOS**;
- `%APPDATA%\amethyst` or `%C:\Users\%USER%\AppData\Roaming\amethyst` on **Windows**.

This folder contains:

- `config.yml`: the user configuration file.
- `color_schemes/`: the user color schemes folder;
- `modules/`: the user modules folder;

### Editing Amethyst configuration file

You can edit the configuration file defined above to configure Amethyst.

### Create new color palettes

Color palettes are yaml files stored in the user color schemes folder defined above.

### Create new modules

A module is just an executable that sends a gem manifest to stdout and get selected gem facets from stdin.

This executable must use the `main` prefix (ie. `main.sh`, `main.py`, etc.), and be located in a dedicated folder in the
user modules folder defined above.

This allows you to add external resources required by your module in this folder (images, scripts, configuration files, etc.)

You can take a look at the
[builtin modules](https://framagit.org/roipoussiere/amethyst/-/tree/master/amethyst/builtin_modules) for inspiration.

## Update documentation

### Minor updates

If you just want to fix a typo, you can use the GitLab built-in interface to directly edit a page and create a pull
request in order to let me include your modification in the next version of Amethyst.

Just select a file from [the project page on Gitlab](https://framagit.org/roipoussiere/amethyst/-/tree/master/)
and click on the *Edit* button. You must have an account on [Framagit](https://framagit.org/).

### Bigger updates

The documentation website is built with [mkDocs](https://www.mkdocs.org/), using the
[material theme](https://squidfunk.github.io/mkdocs-material/).

To install these Python dependencies in a Python virtual environment (you need Python3 installed on your system):

```
make docs-init
```

To build the documentation:

```
make docs
```

Or to both build and serve the documentation on a local website:

```
make docs-serve
```

## Contribute to code

First you can start to find an interesting issue in the
[issue tracker)(https://framagit.org/roipoussiere/amethyst/-/issues), in particular
[those tagged `good first issue`](https://framagit.org/roipoussiere/amethyst/-/issues?label_name%5B%5D=good+first+issue).

### Install Golang

`sudo apt install golang` on Ubuntu. On other systems, please read the
[installation instructions](https://golang.org/doc/install).

### Install system dependencies

This is required by [GLFW](https://www.glfw.org/) to build source code.

Read [installation instructions](https://github.com/go-gl/glfw#installation)
(on Ubuntu: `sudo apt install libgl1-mesa-dev xorg-dev`).

### Install Amethyst for development

```
git clone --recurse-submodules https://framagit.org/roipoussiere/amethyst.git
cd amethyst
make use-pixel-fork
make build
```

Then you can check your installation with `build/bin/amethyst --version`.

### About the Pixel fork

You probably noticed these `--recurse-submodules` and `make use-pixel-fork`.
I contribute to the Pixel library that Amethyst uses, and my modified version is used in Amethyst, before my Pull Requests are merged.
You just downloaded my Pixel fork in the last section with `--recurse-submodules`, and you checkouted the `amethyst` branch with `make use-pixel-fork`.

## Developing

### Fork the project

1. [Fork the Amethys project](https://framagit.org/roipoussiere/amethyst/-/forks/new);
2. update the url of the *origin* remote (you can get it by clicking the *clone* button on your fork page on GitLab):

```
git remote set-url origin https://framagit.org/<your_gitlab_user>/amethyst.git
```

### Write code

[Go by Example](https://gobyexample.com/) is a good start if your are new to Go.

### Test your code

Several tools are available to help you to check your source code, which can be executed with make commands:

- `make check` executes [go vet](https://golang.org/cmd/vet/) to examine source code, and
[golint](https://pkg.go.dev/golang.org/x/lint/golint) to look for style mistakes in source code;
- `make format` executes [gofmt](https://golang.org/cmd/gofmt/) to format source code.

Notes:
- Golint must be installed before with `go get -u golang.org/x/lint/golint` (or `sudo apt install golint` on Ubuntu);
- any of these commands are executed in during the CI for now.

### Use Git Hooks

You may want to automatically executes these commands before each commit with a
[git hook](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks):

```
echo 'make check' > .git/hooks/pre-commit
echo 'make format' >> .git/hooks/pre-commit
```

Notes:
- these commands are executed really fast and should not bother the developper.
- the hook will not block the commit (you must ammend it to fix the report).

### Create a merge request

Create a new git branch, write code, create commits, push to your fork then
[create a merge request](https://framagit.org/roipoussiere/amethyst/-/merge_requests/new)!
