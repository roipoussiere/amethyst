// main.go is the Amethyst app starting point that provides the amethyst command.

package main

import (
	"errors"
	"fmt"
	"framagit.org/roipoussiere/amethyst/modules"
	"github.com/faiface/pixel/pixelgl"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

var (
	// InfoLogger id used to log informational messages for developpers.
	InfoLogger *log.Logger
	// WarningLogger id used to log import messages for developpers, that require attention.
	WarningLogger *log.Logger
	// ErrorLogger id used to log error messages for users and developpers.
	ErrorLogger *log.Logger
	// Config stores the user configuration.
	Config AppConfig
	// ColorScheme store the colors to use in the user interface.
	colorScheme ColorScheme
)

func main() {
	pixelgl.Run(parseCli)
}

func parseCli() {
	var flagVerbose, flagVersion, flagConfig bool

	var rootCmd = &cobra.Command{
		Use:   "amethyst",
		Short: "Amethyst is a general-purpose keyboard-centric radial menu.",
		Long: `Amethyst is a general-purpose keyboard-centric radial menu.
Complete documentation is available at https://roipoussiere.frama.io/amethyst/`,
		RunE: func(cmd *cobra.Command, args []string) error {
			logArgs(cmd)
			switch {
			case flagVerbose:
				enableVerbose()
			case flagVersion:
				printVersion()
			case flagConfig:
				listConfig()
			case len(args) == 0:
				return errors.New("a module name or flag must be provided")
			}
			return nil
		},
	}

	initialize()

	rootCmd.SetHelpCommand(&cobra.Command{Use: "", Hidden: true})
	rootCmd.PersistentFlags().BoolVarP(&flagVerbose, "verbose", "V", false, "Enable verbose mode")
	rootCmd.Flags().BoolVarP(&flagVersion, "version", "v", false, "Show version and exit.")
	rootCmd.Flags().BoolVarP(&flagConfig, "config", "c", false, "Show configuration and exit")

	rootCmd.AddCommand(modules.ModuleRun)

	for moduleName := range getModulesPath(false) {
		rootCmd.AddCommand(&cobra.Command{
			Use:   moduleName,
			Short: "User module " + moduleName,
			Run: func(cmd *cobra.Command, args []string) {
				start(cmd.Use, args)
			},
		})
	}

	re := strings.NewReplacer("command", "module", "Command", "Module")
	rootCmd.SetUsageTemplate(re.Replace(rootCmd.UsageString()))

	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func enableVerbose() {
	InfoLogger = log.New(os.Stdout, "INFO:    ", log.Ltime|log.Lshortfile)
	WarningLogger = log.New(os.Stdout, "WARNING: ", log.Ltime|log.Lshortfile)
	ErrorLogger = log.New(os.Stderr, "ERROR:   ", log.Ltime|log.Lshortfile)
}

func logArgs(cmd *cobra.Command) {
	InfoLogger.Print("🟊 Starting Amethyst 🟊")
	InfoLogger.Print("Root command arguments:")
	cmd.Flags().VisitAll(func(flag *pflag.Flag) {
		InfoLogger.Printf("• %s: %s", flag.Name, flag.Value)
	})
}

func printVersion() {
	fmt.Printf("Amethyst %s\n", AppVersion)
	os.Exit(0)
}

func listConfig() {
	fmt.Println("  • UserResources: " + userResourcesDir)
	initUserResources("config.yml")
	var config AppConfig = GetUserConfig()
	fmt.Println(prettyPrint(config))
	os.Exit(0)
}

func initialize() {
	InfoLogger = log.New(ioutil.Discard, "", 0)
	WarningLogger = log.New(ioutil.Discard, "", 0)
	ErrorLogger = log.New(os.Stderr, "", 0)

	LogSysInfo()
	createUserResourcesDir("modules", "color_schemes")
	initUserResources("config.yml")
	Config = GetUserConfig()
	colorScheme = getColorScheme(Config.ColorScheme)
}
