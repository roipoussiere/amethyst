// config.go contains code related to the Amethyst configuration.

package main

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"path"
	"strings"
)

// AppConfig defines the Amethyst configuration.
type AppConfig struct {
	WindowWidth   float64 `yaml:"window_width"`
	WindowHeight  float64 `yaml:"window_height"`
	InnerRadius   float64 `yaml:"inner_radius"`
	OuterRadius   float64 `yaml:"outer_radius"`
	GapSize       float64 `yaml:"gap_size"`
	IconSize      float64 `yaml:"icon_size"`
	LabelTextSize float64 `yaml:"label_text_size"`
	KeyDelay      int     `yaml:"key_delay"`
	ColorScheme   string  `yaml:"color_scheme"`
}

// GetUserConfig returns the user configuration.
func GetUserConfig() AppConfig {
	var config AppConfig

	// userConfigPath := getResourcePath("config.yml")
	userConfigPath := path.Join(userResourcesDir, "config.yml")
	InfoLogger.Printf("Loading configuration from %v...", userConfigPath)

	source, err3 := ioutil.ReadFile(userConfigPath)
	if err3 != nil {
		ErrorLogger.Fatalf("Can not read user config file: %s", err3)
	}

	err4 := yaml.Unmarshal(source, &config)
	if err4 != nil {
		ErrorLogger.Fatalf("Can not parse user config file: %s", err4)
	}

	InfoLogger.Printf("Loaded configuration:")
	for _, configStringLine := range strings.Split(config.ToString(), "\n") {
		InfoLogger.Printf(configStringLine)
	}
	return config
}

// ToString returns the string representation of a config.
func (config AppConfig) ToString() string {
	strConfig := fmt.Sprintf("  • WindowWidth: %.2f\n", config.WindowWidth)
	strConfig += fmt.Sprintf("  • WindowHeight: %.2f\n", config.WindowHeight)
	strConfig += fmt.Sprintf("  • InnerRadius: %.2f\n", config.InnerRadius)
	strConfig += fmt.Sprintf("  • OuterRadius: %.2f\n", config.OuterRadius)
	strConfig += fmt.Sprintf("  • GapSize: %.2f\n", config.GapSize)
	strConfig += fmt.Sprintf("  • IconSize: %.2f\n", config.IconSize)
	strConfig += fmt.Sprintf("  • LabelTextSize: %.2f\n", config.LabelTextSize)
	strConfig += fmt.Sprintf("  • KeyDelay: %d\n", config.KeyDelay)
	strConfig += fmt.Sprintf("  • ColorScheme: %s", config.ColorScheme)
	return strConfig
}
