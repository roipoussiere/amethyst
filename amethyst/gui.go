// gui.go contains code related to the Graphical User Interface.

package main

import (
	"fmt"
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/pixelgl"
	"github.com/faiface/pixel/text"
	"github.com/golang/freetype/truetype"
	"github.com/llgcode/draw2d"
	"github.com/llgcode/draw2d/draw2dimg"
	"golang.org/x/image/font/gofont/goregular"
	"image"
	"image/color"
	"io/ioutil"
	"math"
	"os"
	"path"
	"sync"
)

// UserInput defines the actions that can be triggered by the user.
type UserInput string

const (
	uiUp    UserInput = "u"
	uiRight UserInput = "r"
	uiDown  UserInput = "d"
	uiLeft  UserInput = "l"
	uiClose UserInput = "c"
	uiNone  UserInput = ""
)

func getCustomTTF(fontName string) []byte {
	file, err := os.Open(path.Join(projectPath, "assets", fontName))
	if err != nil {
		ErrorLogger.Fatalf("Can not open font %s: %s", fontName, err)
	}
	defer file.Close()

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		ErrorLogger.Fatalf("Can not read font %s: %s", fontName, err)
	}
	return bytes
}

func loadAtlas(ttf []byte, fontSize float64, runeSet []rune) *text.Atlas {
	font, err := truetype.Parse(ttf)
	if err != nil {
		ErrorLogger.Fatalf("Can not load font atlas: %s", err)
	}

	face := truetype.NewFace(font, &truetype.Options{Size: fontSize, GlyphCacheEntries: 1})
	return text.NewAtlas(face, runeSet)
}

func startGUI(waitGroup *sync.WaitGroup, queriesChan <-chan Query, userInputsChan chan<- UserInput) {
	defer waitGroup.Done()
	pxWindow := createWindow()

	InfoLogger.Print("Entering GUI loop")
	for !pxWindow.Closed() {
		userInput := getUserInput(pxWindow)
		if userInput != uiNone {
			userInputsChan <- userInput
		}

		select {
		case query := <-queriesChan:
			drawOnWindow(pxWindow, query)
		default:
		}
		pxWindow.Update()
	}
	InfoLogger.Print("Quitted GUI loop")
}

func createWindow() *pixelgl.Window {
	InfoLogger.Print("Creating window")
	var monitorWidth, monitorHeight float64 = pixelgl.PrimaryMonitor().Size()

	config := pixelgl.WindowConfig{
		Title:                  "Amethyst",
		Bounds:                 pixel.R(0, 0, Config.WindowWidth, Config.WindowHeight),
		VSync:                  true,
		Resizable:              false,
		Undecorated:            true,
		TransparentFramebuffer: true,
	}

	win, err := pixelgl.NewWindow(config)
	if err != nil {
		ErrorLogger.Fatalf("Can not create window: %s", err)
	}

	win.SetPos(pixel.V(monitorWidth/2-Config.WindowWidth/2, monitorHeight/2-Config.WindowHeight/2))
	return win
}

func getUserInput(win *pixelgl.Window) UserInput {
	if win.JustPressed(pixelgl.KeyLeft) {
		return uiLeft
	} else if win.JustPressed(pixelgl.KeyRight) {
		return uiRight
	} else if win.JustPressed(pixelgl.KeyUp) {
		return uiUp
	} else if win.JustPressed(pixelgl.KeyDown) {
		return uiDown
	} else if win.JustPressed(pixelgl.KeyEscape) {
		return uiClose
	}
	return uiNone
}

func drawOnWindow(pxWindow *pixelgl.Window, query Query) {
	InfoLogger.Print("Updating window content")

	pxWindow.Clear(colorScheme.WindowBg)
	// pxWindow.Clear(colornames.Black)
	createGemSprite(query).Draw(pxWindow, pixel.IM.Moved(pxWindow.Bounds().Center()))
	drawIcons(pxWindow, query)
	drawLabels(pxWindow, query)
}

func getIconsContentFromFacet(facets []Facet) (map[FacetPosition]string, map[FacetPosition]string) {
	InfoLogger.Print("Getting icons:")
	iconsContent := make(map[FacetPosition]string)
	textsContent := make(map[FacetPosition]string)

	for _, facet := range facets {
		if facet.MaterialIcon != "" {
			if icon, ok := materialIcons[facet.MaterialIcon]; ok {
				InfoLogger.Printf("  • %s: Using icon %s.", facet.Position, string(icon))
				iconsContent[facet.Position] = string(icon)
				continue
			}
			WarningLogger.Printf("Icon '%s' not found, using text icon instead", facet.MaterialIcon)
		}
		if facet.TextIcon != "" {
			if len(facet.TextIcon) > 2 {
				WarningLogger.Printf("Text icon content '%s' is too long, keeping only 2 first letters.", facet.TextIcon)
				facet.TextIcon = facet.TextIcon[:2]
			}
			InfoLogger.Printf("  • %s: Using text icon '%s'.", facet.Position, facet.TextIcon)
			textsContent[facet.Position] = facet.TextIcon
		} else {
			WarningLogger.Printf("  • %s: No icon provided, using text icon based on label ('%s' -> '%s').",
				facet.Position, facet.Label, shorten(facet.Label))
			textsContent[facet.Position] = shorten(facet.Label)
		}
	}
	return iconsContent, textsContent
}

func drawIcons(pxWindow *pixelgl.Window, query Query) {
	iconsRadius := Config.InnerRadius + (Config.OuterRadius-Config.InnerRadius)/2
	iconsContent, textsContent := getIconsContentFromFacet(query.Facets)

	if len(iconsContent) > 0 {
		fontAtlas := loadAtlas(getCustomTTF("MaterialIcons-Regular.ttf"), Config.IconSize, getMaterialIconsRunes())
		drawTexts(pxWindow, iconsContent, iconsRadius, fontAtlas, false, colorScheme.FacetsIcons, colorScheme.FacetsIconsBg)
	}
	if len(textsContent) > 0 {
		fontAtlas := loadAtlas(goregular.TTF, Config.IconSize, text.ASCII)
		drawTexts(pxWindow, textsContent, iconsRadius, fontAtlas, false, colorScheme.FacetsIcons, colorScheme.FacetsIconsBg)
	}
}

func drawLabels(pxWindow *pixelgl.Window, query Query) {
	fontAtlas := loadAtlas(goregular.TTF, Config.LabelTextSize, text.ASCII)
	labelsRadius := Config.OuterRadius + fontAtlas.LineHeight()

	textContents := make(map[FacetPosition]string)
	for _, facet := range query.Facets {
		textContents[facet.Position] = facet.Label
	}

	drawTexts(pxWindow, textContents, labelsRadius, fontAtlas, true, colorScheme.FacetsLabel, colorScheme.FacetsLabelBg)
}

func drawTexts(pxWindow *pixelgl.Window, textContents map[FacetPosition]string, radius float64, atlas *text.Atlas, onEdge bool, color, backgroundColor color.Color) {
	InfoLogger.Printf("Drawing text on each facets at radius %.2f:", radius)
	var anchor pixel.Anchor

	for facetPos, textContent := range textContents {
		if textContent != "" {
			textXPos := Config.WindowWidth/2 + radius*math.Cos(RadialPositions[facetPos])
			textYPos := Config.WindowHeight/2 + radius*math.Sin(RadialPositions[facetPos])
			if onEdge {
				anchor = anchorPositions[facetPos]
			} else {
				anchor = pixel.Center
			}
			InfoLogger.Printf("  • '%s' at position [%.2f; %.2f] aligned on %s.", textContent, textXPos, textYPos, anchor.String())
			pxText := text.New(pixel.V(textXPos, textYPos), atlas)
			pxText.Color = color
			fmt.Fprint(pxText, textContent)

			rect := pxText.Bounds().AlignedTo(anchor).Moved(pixel.V(0, atlas.Descent()))

			bgPadding := pxText.BoundsOf("A").Size().X
			rect = rect.Resized(rect.Center(), rect.Size().Add(pixel.V(bgPadding, bgPadding)))
			drawRect(pxWindow, rect, backgroundColor)

			pxText.AlignedTo(anchor).Draw(pxWindow, pixel.IM)
		}
	}
}

func drawRect(pxWindow *pixelgl.Window, rect pixel.Rect, color color.Color) {
	imd := imdraw.New(nil)
	imd.Color = color
	imd.Push(rect.Min, rect.Max)
	imd.Rectangle(0)
	imd.Draw(pxWindow)
}

func createGemSprite(query Query) *pixel.Sprite {
	facetsPositions, ok := facetsPositionbyMode[query.GemAttrs.Mode]
	if !ok {
		WarningLogger.Printf("gem mode '%s' is not recognised, using octoGem instead.", query.GemAttrs.Mode)
		facetsPositions = octoGemPos
	}

	img := image.NewRGBA(image.Rect(0, 0, int(Config.WindowWidth), int(Config.WindowHeight)))
	gc := draw2dimg.NewGraphicContext(img)
	gc.BeginPath()
	gc.SetFillColor(colorScheme.FacetsBg)

	winCenterX, winCenterY := Config.WindowWidth/2, Config.WindowHeight/2
	outerGapAngle := math.Asin(Config.GapSize / 2 / Config.OuterRadius)
	innerGapAngle := math.Asin(Config.GapSize / 2 / Config.InnerRadius)
	angle := 2 * math.Pi / float64(len(facetsPositions))

	InfoLogger.Print("Drawing facets:")
	for i, facetPos := range facetsPositions {
		facet := getFacetByPos(query.Facets, facetPos)
		if facet != nil {
			startAngle := float64(i) * angle //+ math.Pi/2
			InfoLogger.Printf("  • %s at angle %.2f°", facet.ToString(), startAngle*180/math.Pi)

			path := new(draw2d.Path)
			path.ArcTo(winCenterX, winCenterY, Config.OuterRadius, Config.OuterRadius, startAngle-angle/2+outerGapAngle, angle-2*outerGapAngle)
			path.ArcTo(winCenterX, winCenterY, Config.InnerRadius, Config.InnerRadius, startAngle+angle/2-innerGapAngle, -angle+2*innerGapAngle)
			path.Close()
			gc.Fill(path)
		} else {
			InfoLogger.Printf("  • %s is skiped", facetPos)
		}
	}

	picture := pixel.PictureDataFromImage(img)
	return pixel.NewSprite(picture, picture.Bounds())
}
