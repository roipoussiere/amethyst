// queries.go contains code related to modules queries.

package main

import (
	"encoding/json"
	"strings"
)

// Query defines a gem instruction, usually sent by a module to update the gem.
type Query struct {
	// APIVersion string     `json:"api_version"`
	Commands []ICommand `json:"commands"`
	GemAttrs IGemAttrs  `json:"gem_attrs"`
	Facets   []Facet    `json:"facets"`
	Error    string     `json:"error"`
}

func newErrorQuery(error string) Query {
	return Query{Commands: nil, GemAttrs: IGemAttrs{Mode: ""}, Facets: nil, Error: error}
}

// IGemAttrs defines the gem attributes that can be defined in a module query.
type IGemAttrs struct {
	Mode IGMode `json:"mode"`
}

// ICommand lists the commands that can be defined in a module query.
type ICommand string

const (
	icQuit  ICommand = "quit"
	icClear ICommand = "clear"
)

func (query *Query) containsCommand(command ICommand) bool {
	for _, otherCommand := range query.Commands {
		if command == otherCommand {
			return true
		}
	}
	return false
}

// IGMode lists the gem types that can be defined in the module type attribute
type IGMode string

const (
	igmDuo    IGMode = "duo"
	igmQuadro IGMode = "quadro"
	igmOcto   IGMode = "octo"
)

// ParseQuery parses a module query formated in json, and returns an Instruction struct.
func ParseQuery(jsonString string) Query {
	var query Query
	err := json.Unmarshal([]byte(jsonString), &query)
	if err != nil {
		ErrorLogger.Fatalf("Can not parse module query: %s", err)
	}
	return query
}

// ToString returns the string representation of a Query.
func (query *Query) ToString() string {
	strQuery := ""

	var strCommands []string
	for _, command := range query.Commands {
		strCommands = append(strCommands, string(command))
	}
	commands := strings.Join(strCommands, ", ")
	if commands == "" {
		commands = "∅"
	}
	strQuery += "  • commands: " + commands + "\n"

	strQuery += "  • attributes:\n"
	strQuery += "    • mode: " + string(query.GemAttrs.Mode) + "\n"

	strQuery += "  • facets: \n"
	for _, facet := range query.Facets {
		strQuery += "    • " + facet.ToString() + "\n"
	}

	if query.Error == "" {
		strQuery += "  • error: ∅"
	} else {
		strQuery += "  • error: " + query.Error
	}

	return strQuery
}
