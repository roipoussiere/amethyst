package main

import (
	"io"
	"os"
	"sync"
)

// inputsToFacetPositions associates user inputs to facet positions.
var inputsToFacetPositions map[UserInput]FacetPosition = map[UserInput]FacetPosition{
	uiUp:    fpNorth,
	uiRight: fpEast,
	uiDown:  fpSouth,
	uiLeft:  fpWest,
}

func start(moduleName string, args []string) {
	var waitGroup sync.WaitGroup
	var moduleStdin io.WriteCloser
	var moduleStdout, moduleStderr io.ReadCloser

	if moduleName == "-" {
		InfoLogger.Print("Launching module from stdin.")
		moduleStdin, moduleStdout, moduleStderr = os.Stdout, os.Stdin, os.Stderr
	} else {
		moduleStdin, moduleStdout, moduleStderr = startModule(moduleName, args)
	}

	userInputsChan := make(chan UserInput)
	queriesChan := make(chan Query)
	responsesChan := make(chan FacetPosition)
	errorsChan := make(chan string)

	waitGroup.Add(1)
	go startGUI(&waitGroup, queriesChan, userInputsChan)

	go handleUserInputs(userInputsChan, responsesChan)
	go handleModuleErrors(errorsChan)

	go connectModuleStdin(responsesChan, moduleStdin)
	go connectModuleStdout(moduleStdout, queriesChan, errorsChan)
	go connectModuleStderr(moduleStderr, errorsChan)

	waitGroup.Wait()
}

// handleUserInputs reads the userInputsChan and executes actions accordingly (ie. pass a facet position to responsesChan).
func handleUserInputs(userInputsChan <-chan UserInput, responsesChan chan<- FacetPosition) {
	for userInput := range userInputsChan {
		InfoLogger.Print("user input: " + userInput)
		if userInput == uiClose {
			InfoLogger.Printf("User asked to exit.")
			os.Exit(0)
		}
		responsesChan <- inputsToFacetPositions[userInput]
	}
}

// handleModuleErrors reads the errorsChan and executes actions accordingly (ie. quit Amethyst with error).
func handleModuleErrors(errorsChan <-chan string) {
	error := <-errorsChan
	ErrorLogger.Fatal("Module returned: " + error)
	os.Exit(1)
}
