// app_info.go defines meta-information about Amethyst, that can be set by external tools.

package main

const (
	// AppVersion defines the current version of the application (value is set automatically with `make version`)
	AppVersion string = "0.4.6"

	// AppName defines the name the application (used for instance to build user config path).
	AppName string = "amethyst"
)
