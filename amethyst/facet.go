// facet.go contains code related to the Facet struct.

package main

import (
	"github.com/faiface/pixel"
	"math"
)

// Facet represents a gem facet, that can be chosed by the user.
type Facet struct {
	Position     FacetPosition `json:"pos"`
	Label        string        `json:"label"`
	MaterialIcon string        `json:"icon.material"`
	TextIcon     string        `json:"icon.text"`
}

// FacetPosition enumerates the positions of a facet in the gem (north, south, etc.).
type FacetPosition string

const (
	fpEast      FacetPosition = "east"
	fpNorthEast FacetPosition = "north-east"
	fpNorth     FacetPosition = "north"
	fpNorthWest FacetPosition = "north-west"
	fpWest      FacetPosition = "west"
	fpSouthWest FacetPosition = "south-west"
	fpSouth     FacetPosition = "south"
	fpSouthEast FacetPosition = "south-east"
)

// RadialPositions defines the radial position (in radians) of each facet position.
var RadialPositions map[FacetPosition]float64 = map[FacetPosition]float64{
	fpEast:      0.0,
	fpNorthEast: math.Pi / 4 * 1,
	fpNorth:     math.Pi / 4 * 2,
	fpNorthWest: math.Pi / 4 * 3,
	fpWest:      math.Pi / 4 * 4,
	fpSouthWest: math.Pi / 4 * 5,
	fpSouth:     math.Pi / 4 * 6,
	fpSouthEast: math.Pi / 4 * 7,
}

var anchorPositions map[FacetPosition]pixel.Anchor = map[FacetPosition]pixel.Anchor{
	fpEast:      pixel.Right,
	fpNorthEast: pixel.TopRight,
	fpNorth:     pixel.Top,
	fpNorthWest: pixel.TopLeft,
	fpWest:      pixel.Left,
	fpSouthWest: pixel.BottomLeft,
	fpSouth:     pixel.Bottom,
	fpSouthEast: pixel.BottomRight,
}

// GemMode enumerates the gem modes (Duo, Quadro, Octo)
type GemMode []FacetPosition

var (
	duoGemPos    GemMode = []FacetPosition{fpEast, fpWest}
	quadroGemPos GemMode = []FacetPosition{fpEast, fpNorth, fpWest, fpSouth}
	octoGemPos   GemMode = []FacetPosition{fpEast, fpNorthEast, fpNorth, fpNorthWest,
		fpWest, fpSouthWest, fpSouth, fpSouthEast}
	facetsPositionbyMode map[IGMode]GemMode = map[IGMode]GemMode{igmDuo: duoGemPos, igmQuadro: quadroGemPos, igmOcto: octoGemPos}
)

func getFacetByPos(facets []Facet, facetPos FacetPosition) *Facet {
	for _, otherFacet := range facets {
		if facetPos == otherFacet.Position {
			return &otherFacet
		}
	}
	return nil
}

// NewFacet creates a new facet at a given position, with "∅" as label.
func NewFacet(position FacetPosition) Facet {
	return Facet{Position: position, Label: "∅"}
}

// ToString returns the string reprensentation of a facet.
func (facet *Facet) ToString() string {
	if facet == nil {
		return "∅"
	}
	return string(facet.Position) + ": " + facet.Label
}
