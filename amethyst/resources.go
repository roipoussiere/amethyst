// config.go contains code related to resources management (ie. config, modules and colorschemes).

package main

import (
	"github.com/kirsle/configdir"
	"github.com/otiai10/copy"
	"os"
	"path"
	"runtime"
	"strings"
)

var (
	userResourcesDir    string = getUserResourcesDir()
	builtinResourcesDir string = getBuiltinResourcesDir()
)

// getUserResourcesDir returns the path of the user resources folder.
func getUserResourcesDir() string {
	resourcesDir := configdir.LocalConfig(AppName)
	err := configdir.MakePath(resourcesDir)
	if err != nil {
		ErrorLogger.Fatalf("Can not make user resources folder: %s", err)
	}
	return resourcesDir
}

// getBuiltinResourcesDir returns the path of the builtin resources folder.
func getBuiltinResourcesDir() string {
	_, filename, _, ok := runtime.Caller(1)
	if !ok {
		ErrorLogger.Fatal("Can not get builtin config folder.")
	}
	return path.Join(path.Dir(path.Dir(filename)), "resources")
}

// getResourcePath gets the given resource path, relative to the user resource folder, or builtin resource folder as fallback.
func getResourcePath(resourceRelPath string) string {
	InfoLogger.Printf("Looking for resource path %s...", resourceRelPath)
	userResourcePath := path.Join(userResourcesDir, resourceRelPath)
	builtinResourcePath := path.Join(builtinResourcesDir, resourceRelPath)
	if _, err := os.Stat(userResourcePath); !os.IsNotExist(err) {
		InfoLogger.Print("  ⮡ found in user resource folder: " + userResourcePath)
		return userResourcePath
	} else if _, err := os.Stat(builtinResourcePath); !os.IsNotExist(err) {
		InfoLogger.Print("  ⮡ found in builtin resource folder: " + builtinResourcePath)
		return builtinResourcePath
	}
	ErrorLogger.Fatal("Can not find resource " + resourceRelPath + " in user resources folder, neither in builtin resources folder.")
	return ""
}

// initUserResources copies content relative to builtin resources folder into user's resources folder if it doesn't exist.
func initUserResources(resourcesRelPath ...string) {
	InfoLogger.Println("Checking user resources " + strings.Join(resourcesRelPath, ", ") + "...")
	for _, resource := range resourcesRelPath {
		userResourcePath := path.Join(userResourcesDir, resource)
		if _, err := os.Stat(userResourcePath); os.IsNotExist(err) {
			builtinResourcePath := path.Join(builtinResourcesDir, resource)
			err := copy.Copy(builtinResourcePath, userResourcePath)
			if err != nil {
				ErrorLogger.Fatalf("Can not copy builtin resources to %s: %s.", userResourcePath, err)
			}
			InfoLogger.Printf("  • copied builtin resource %s to user resource %s.", builtinResourcePath, userResourcePath)
		}
	}
}

// createUserResourcesDir create given empty folders in user resources folder if it doesn't exist.
func createUserResourcesDir(dirsName ...string) {
	InfoLogger.Printf("Checking user resources folders %s...", strings.Join(dirsName, ", "))

	for _, dirName := range dirsName {
		userResourceDir := path.Join(userResourcesDir, dirName)
		if _, err := os.Stat(userResourceDir); os.IsNotExist(err) {
			os.MkdirAll(userResourceDir, os.ModePerm)
			InfoLogger.Printf("  • created new user resource folder %s.", userResourceDir)
		}
	}
}
