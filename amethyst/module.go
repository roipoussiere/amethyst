// module.go contains code related to the modules.

package main

import (
	"bufio"
	"encoding/json"
	"io"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
)

// Return the absolute path of a given module
func getCmdPathFromModuleName(moduleName string) string {
	if modulePath, ok := getModulesPath(true)[moduleName]; ok {
		return modulePath
	}
	if modulePath, ok := getModulesPath(false)[moduleName]; ok {
		return modulePath
	}
	ErrorLogger.Fatalf("Module %s is not found, neither in user nor builtin module directory.", moduleName)
	return ""
}

func getCmdPathFromModuleArg(moduleArg string) string {
	var moduleCmdPath string
	var err error
	if moduleArg[0:1] == "." || moduleArg[0:1] == string(os.PathSeparator) {
		moduleCmdPath, err = filepath.Abs(moduleArg)
		if err != nil {
			ErrorLogger.Fatalf("Can not get module absolute path of %s: %s", moduleArg, err)
		}
	} else {
		moduleCmdPath = getCmdPathFromModuleName(moduleArg)
	}
	InfoLogger.Printf("Found module %s executable: %s.", moduleArg, moduleCmdPath)
	return moduleCmdPath
}

func startModule(modulePath string, moduleOptions []string) (io.WriteCloser, io.ReadCloser, io.ReadCloser) {
	var moduleStdin io.WriteCloser
	var moduleStdout, moduleStderr io.ReadCloser
	var err error

	InfoLogger.Printf("Launching module %s %s", modulePath, strings.Join(moduleOptions, " "))
	cmd := exec.Command(getCmdPathFromModuleArg(modulePath), moduleOptions...)

	moduleStdin, err = cmd.StdinPipe()
	if err != nil {
		ErrorLogger.Fatalf("Can not obtain module stdin: %s", err)
	}

	moduleStdout, err = cmd.StdoutPipe()
	if err != nil {
		ErrorLogger.Fatalf("Can not obtain module stdout: %s", err)
	}

	moduleStderr, err = cmd.StderrPipe()
	if err != nil {
		ErrorLogger.Fatalf("Can not obtain module stderr: %s", err)
	}

	InfoLogger.Print("Starting Module")
	err = cmd.Start()
	if err != nil {
		ErrorLogger.Fatalf("Can not start module: %s", err)
	}

	return moduleStdin, moduleStdout, moduleStderr
}

func connectModuleStdin(responsesChan <-chan FacetPosition, moduleStdin io.WriteCloser) {
	InfoLogger.Print("Connecting module stdin")
	defer moduleStdin.Close()

	for facePosition := range responsesChan {
		InfoLogger.Print("Sending to module stdin: " + facePosition)
		moduleStdin.Write([]byte(facePosition + "\n"))
	}
}

func connectModuleStdout(moduleStdout io.ReadCloser, queriesChan chan<- Query, errorsChan chan<- string) {
	InfoLogger.Print("Connecting module stdout")
	defer moduleStdout.Close()

	var query Query
	dec := json.NewDecoder(moduleStdout)
	for {
		err := dec.Decode(&query)

		if err != io.EOF && err != nil {
			ErrorLogger.Fatalf("Can not decode module query: %s", err)
		}

		InfoLogger.Printf("Parsed module query:")
		for _, queryStringLine := range strings.Split(query.ToString(), "\n") {
			InfoLogger.Printf(queryStringLine)
		}
		queriesChan <- query

		if query.Error != "" {
			errorsChan <- query.Error
			break
		}

		if err == io.EOF || query.containsCommand(icQuit) {
			break
		}
	}
}

func connectModuleStderr(moduleStderr io.ReadCloser, errorsChan chan<- string) {
	InfoLogger.Print("Connecting module stderr")
	defer moduleStderr.Close()

	scanner := bufio.NewScanner(bufio.NewReader(moduleStderr))
	for scanner.Scan() {
		moduleError := scanner.Text()
		InfoLogger.Printf("Module stderr: %s", moduleError)

		errorsChan <- moduleError
	}
	InfoLogger.Print("Closing stderr")
}

func getModulesPath(userOnly bool) map[string]string {
	modulesPath := make(map[string]string, 0)

	resourcesDir := path.Join(builtinResourcesDir, "modules")
	if userOnly {
		resourcesDir = path.Join(userResourcesDir, "modules")
	}

	err := filepath.Walk(resourcesDir, func(filePath string, f os.FileInfo, walkErr error) error {
		if strings.TrimSuffix(filepath.Base(filePath), filepath.Ext(filePath)) == "main" {
			moduleRelDir := strings.Replace(path.Dir(filePath), resourcesDir+"/", "", 1)
			moduleName := strings.Replace(filepath.ToSlash(moduleRelDir), "/", ".", -1)
			modulesPath[moduleName] = filePath
		}
		return walkErr
	})
	if err != nil {
		ErrorLogger.Fatalf("Can not inspect folder %s: %s", resourcesDir, err)
	}

	return modulesPath
}
