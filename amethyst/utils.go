// utils.go is used to provide global utils functions for various needs.

package main

import (
	"fmt"
	"github.com/elastic/go-sysinfo"
	"path/filepath"
	"runtime"
	"strings"
)

var (
	_, b, _, _         = runtime.Caller(0)
	projectPath string = filepath.Dir(filepath.Dir(b))
)

// LogSysInfo logs various information about the host system, useful for debugging purpose.
func LogSysInfo() {
	host, err := sysinfo.Host()
	if err != nil {
		ErrorLogger.Fatalf("Can not get system info: %s.", err)
	}
	osInfo := host.Info()

	InfoLogger.Printf("System information:")
	InfoLogger.Printf("  • System architecture: %s", osInfo.Architecture)
	InfoLogger.Printf("  • OS: %s %s %s", osInfo.OS.Family, osInfo.OS.Name, osInfo.OS.Version)
	InfoLogger.Printf("  • Kernel version: %s", osInfo.KernelVersion)
	InfoLogger.Printf("  • Go version: %s", runtime.Version())
	InfoLogger.Printf("  • Amethyst version: %s", AppVersion)
}

func prettyPrint(i interface{}) string {
	txt := fmt.Sprintf("%+v\n", i)
	txt = strings.Replace(txt[1:len(txt)-2], " ", "\n  • ", -1)
	return "  • " + strings.Replace(txt, ":", ": ", -1)
}

func shorten(text string) string {
	labelWords := strings.Split(text, " ")
	if len(labelWords) == 1 {
		return strings.ToUpper(labelWords[0][0:1]) + strings.ToLower(labelWords[0][1:1])
	}
	return strings.ToUpper(labelWords[0][0:1]) + strings.ToLower(labelWords[1][0:1])
}
