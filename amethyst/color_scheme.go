// color_scheme.go contains code related to color schemes.

package main

import (
	"encoding/hex"
	"fmt"
	"gopkg.in/yaml.v2"
	"image/color"
	"io/ioutil"
	"path"
	"strings"
)

// RawColorScheme defines the colors used in the user interface, as colorscheme file raw values.
type RawColorScheme struct {
	WindowBg      string `yaml:"window background"`
	FacetsBg      string `yaml:"facets background"`
	FacetsLabel   string `yaml:"facets label"`
	FacetsLabelBg string `yaml:"facets label background"`
	FacetsIcons   string `yaml:"facets icons"`
	FacetsIconsBg string `yaml:"facets icons background"`
}

// ColorScheme defines the colors used in the user interface, as actual colors.
type ColorScheme struct {
	WindowBg      color.Color
	FacetsBg      color.Color
	FacetsLabel   color.Color
	FacetsLabelBg color.Color
	FacetsIcons   color.Color
	FacetsIconsBg color.Color
}

func parseColor(colorStr string) color.Color {
	var err error
	var hexColors []byte

	switch len(colorStr) {
	case 9:
		hexColors, err = hex.DecodeString(colorStr[1:9])
		if err == nil {
			return color.RGBA{hexColors[0], hexColors[1], hexColors[2], hexColors[3]}
		}
	case 7:
		hexColors, err = hex.DecodeString(colorStr[1:7])
		if err == nil {
			return color.RGBA{hexColors[0], hexColors[1], hexColors[2], 255}
		}
	}

	WarningLogger.Printf("Can not parse color '%s', using grey as fallback (supported formats: '#RRGGBB' or '#RRGGBBAA'): %s", colorStr, err)
	return color.RGBA{125, 125, 125, 255}
}

func colorToString(color color.Color) string {
	r, g, b, a := color.RGBA()
	return "#" + hex.EncodeToString([]byte{byte(r), byte(g), byte(b), byte(a)})
}

// ToColorScheme parse color values of RawColorScheme and returns a ColorScheme.
func (rawColorScheme RawColorScheme) ToColorScheme() ColorScheme {
	return ColorScheme{
		WindowBg:      parseColor(rawColorScheme.WindowBg),
		FacetsBg:      parseColor(rawColorScheme.FacetsBg),
		FacetsLabel:   parseColor(rawColorScheme.FacetsLabel),
		FacetsLabelBg: parseColor(rawColorScheme.FacetsLabelBg),
		FacetsIcons:   parseColor(rawColorScheme.FacetsIcons),
		FacetsIconsBg: parseColor(rawColorScheme.FacetsIconsBg),
	}
}

// ToString returns the string representation of a ColorScheme.
func (colorScheme ColorScheme) ToString() string {
	strColorScheme := fmt.Sprintf("  • WindowBg: %s\n", colorToString(colorScheme.WindowBg))
	strColorScheme += fmt.Sprintf("  • FacetsBg: %s\n", colorToString(colorScheme.FacetsBg))
	strColorScheme += fmt.Sprintf("  • FacetsLabel: %s\n", colorToString(colorScheme.FacetsLabel))
	strColorScheme += fmt.Sprintf("  • FacetsLabelBg: %s\n", colorToString(colorScheme.FacetsLabelBg))
	strColorScheme += fmt.Sprintf("  • FacetsIcons: %s\n", colorToString(colorScheme.FacetsIcons))
	strColorScheme += fmt.Sprintf("  • FacetsIconsBg: %s", colorToString(colorScheme.FacetsIconsBg))
	return strColorScheme
}

func getColorScheme(colorSchemeName string) ColorScheme {
	var rawColorScheme RawColorScheme

	colorSchemePath := getResourcePath(path.Join("color_schemes", colorSchemeName+".yml"))
	InfoLogger.Printf("Loading color scheme from %v...", colorSchemePath)

	source, err3 := ioutil.ReadFile(colorSchemePath)
	if err3 != nil {
		ErrorLogger.Fatalf("Can not read color scheme file: %s", err3)
	}

	err4 := yaml.Unmarshal(source, &rawColorScheme)
	if err4 != nil {
		ErrorLogger.Fatalf("Can not parse color scheme file: %s", err4)
	}

	colorScheme := rawColorScheme.ToColorScheme()

	InfoLogger.Printf("Loaded color scheme:")
	for _, colorSchemeStringLine := range strings.Split(colorScheme.ToString(), "\n") {
		InfoLogger.Printf(colorSchemeStringLine)
	}
	return colorScheme
}
