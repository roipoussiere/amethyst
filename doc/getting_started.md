# Amethyst

*A general-purpose keyboard-centric radial menu.*

<!-- 
More complete instructions about Amethyst is available on [the documentation website](https://roipoussiere.frama.io/amethyst/).
 -->

!!! note
    This is a work in progress, don't expect anything interesting for now.
    You probably want to bookmark this page and to come back later. ;)

## Installation

Installation process will be improved later. Keep it mind that this project is still under development.

### Download binary

An executable of current development version of Amethyst is built on each project update:

- [Linux](https://roipoussiere.frama.io/amethyst/bin/amethyst)

Windows and MacOS executables are not available for now, please install with the Go command as described above.

### With Go command (recommended)

This procedure is recommended for multi-platform support, since it builds Amethyst from sources.

First install Go. `sudo apt install golang` on Ubuntu. On other systems, please read the
[installation instructions](https://golang.org/doc/install).

```
go get framagit.org/roipoussiere/amethyst
```

The `amethyst` command should now be available from `$GOPATH/bin/amethyst`. You can also add `$GOPATH/bin` in your
system `$PATH` to be able to run the command `amethyst` from anywhere.

### From sources

Please read the section [Install Amethyst for development](./contributing.md#install-amethyst-for-development) in
the contribution guide.

## Usage

```
amethyst examples.api
```

## Contributing

The [contribution guide](./contributing.md) is waiting for you!

## Credits

- author: Nathanaël Jourdane and contributors ([you?](./contributing.md))
- license: [MIT](./LICENSE)
