# Amethyst

<p style="text-align: center"><em>A general-purpose keyboard-centric radial menu.</em></p>

<img style="display: block; margin: auto;" src="./assets/logo.png">

Amethyst displays a user interface (called gem) composed of several choices (called facets), then executes an action
accordingly to what has been selected.

The key point of the user interface is that it is intentionally very limited:

- no mouse support;
- maximum height items to select;
- no long text allowed;
- modules can not take action on the look & feel.

These limitations have some side effects:
- modules developers must create concise and simple gems;
- users discovers available actions and executes tasks quickly.

Seems interesting? Read the [getting started](./getting_started.md) guide!
